const http = require('http');
const db = require('./database');
const app = require('./application');

const server = http.createServer(app);
const mongoUrl = process.env.DB_URL;
const port = process.env.PORT;

exports.listen = done => db.connect(mongoUrl, () => server.listen(port, done));
exports.close = done => server.close(done);