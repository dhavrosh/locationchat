const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const path = require('path');
const router = require('./router');

const app = express();
const envMode = process.env.MODE || 'development';

dotenv.config({ path: `./config/env/.${envMode}` });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, '../public')));
app.use(router);
app.use((err, req, res, next) => res.status(err.status || 500).end(err));

module.exports = app;