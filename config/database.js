const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

require('../app/users/model');
require('../app/messages/model');

exports.connect = (url, done) => mongoose.connect(url, done);