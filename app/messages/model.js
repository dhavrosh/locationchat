const mongoose = require('mongoose');
const locations = require('../utils').getLocations();

const required = { type: String, required: true };

const messageSchema = new mongoose.Schema({
    author: required,
    data: required,
    location: { type: String, required: true, enum: locations },
    dateCreated: { type: Date, default: Date.now },
});

mongoose.model('Message', messageSchema);