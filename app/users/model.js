const mongoose = require('mongoose');
const locations = require('../utils').getLocations();

const required = { type: String, required: true };

const userSchema = new mongoose.Schema({
    name: required,
    email: required ,
    password: required,
    location: { type: String, required: true, enum: locations }
});

mongoose.model('User', userSchema);