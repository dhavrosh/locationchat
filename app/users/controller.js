const User = require('mongoose').model('User');
const sendJSON = require('../utils').sendJSON;

function create(req, res) {
    return User.create(req.body)
        .then(user => sendJSON(res, 200, user))
        .catch(err => sendJSON(res, 500, err.message));
}

function update(req, res) {
    const id = req.params.id;

    if (id) {
        return User.findByIdAndUpdate(id, req.body, { new: true })
            .then(user => sendJSON(res, 200, user))
            .catch(err => sendJSON(res, 500, err.message));
    } else {
        sendJSON(res, 500, 'Id for User is required');
    }
}

module.exports = {
    create,
    update
};