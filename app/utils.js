exports.getLocations = () => {
    return  [
        'Lviv',
        'Kyiv',
        'Odessa',
        'Kharkiv',
        'Dnipro'
    ];
};

exports.sendJSON = (res, code, content) => {
    res.status(code).json(content);
};