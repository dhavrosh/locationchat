const gulp = require('gulp');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const annotate = require('gulp-ng-annotate');
const runSequence = require('run-sequence');
const pump = require('pump');

const src = './public/components/**/*.js';
const dst = './public/js';

gulp.task('build-web-app', (done) => {
  pump([
    gulp.src(src),
    concat('app.min.js'),
    annotate(),
    babel({ presets: ['es2015'] }),
    uglify({ mangle: true }),
    gulp.dest(dst),
  ], done);
});

gulp.task('watch', () => {
  gulp.watch(src, ['build-web-app']);
});

gulp.task('default', (done) => {
  runSequence('build-web-app', 'watch', done);
});
