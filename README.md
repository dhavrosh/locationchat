# messaging web-app on node.js with the following functionality:

  - User can register via email/password

  - User can sign in and edit his profile's email, password, name and select location from predefined list

  - User can broadcast message, and all users within his current location will see it

  - In case user changes his location his old messages will remain visible to the previous location and won't move to a new one

# add process.ENV validation
# repair user update with locations